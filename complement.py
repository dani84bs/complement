#Copyright (c) 2012 Daniele Trainini and other contributors,
#
#Permission is hereby granted, free of charge, to any person obtaining
#a copy of this software and associated documentation files (the
#"Software"), to deal in the Software without restriction, including
#without limitation the rights to use, copy, modify, merge, publish,
#distribute, sublicense, and/or sell copies of the Software, and to
#permit persons to whom the Software is furnished to do so, subject to
#the following conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
#LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
#OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""Complement implementation."""

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from random import randint

GRAY = [1, 1, 1, 1]
BLACK = [0, 0, 0, 0]


class Cell(Button):
    """Represent a cell in the game."""

    def __init__(self, row, col):
        """
        Arguments:
        -row: The cell row on the board
        -col: The cell column on the board
        """

        super(Cell, self).__init__()
        self._state = -1
        self.background_color = GRAY
        self.text = "0"
        self.crow = row
        self.ccol = col

    def on_release(self):
        """Override on_release event handler."""
        self.text = str(int(self.text) + 1)

    def complement(self):
        """Switch the cell status."""
        self._state *= -1
        if self._state == -1:
            self.background_color = GRAY
        else:
            self.background_color = BLACK


class Board(GridLayout):
    """Represent the game board."""

    def __init__(self, w, h, **kwargs):
        """
        Arguments:
        - w: the board width
        - h: the board height
        - kwargs: a dict of arguments that will be passed to super
        """

        super(Board, self).__init__(cols=w)
        self.cw = w
        self.ch = h
        self._board = [[Cell(r, c) for c in xrange(w)] for r in xrange(h)]

        for line in self._board:
            for cell in line:
                self.add_widget(cell)

    def clear(self):
        """Clear the board."""

        for line in self._board:
            for cell in line:
                cell.text = "0"
                cell._state = -1
                cell.background_color = GRAY

    def move(self, row, col):
        """
        Do a move on the board.
        Arguments:
        -row: The row of the cell on wich apply the move
        -col: The column of the cell on wich apply the move
        """

        self._board[row][col].complement()
        self._board[(row + 1) % self.ch][col].complement()
        self._board[row][(col + 1) % self.cw].complement()
        self._board[row - 1][col].complement()
        self._board[row][col - 1].complement()

    def generate_level(self, d):
        """Generate a level of difficulty d."""

        tmp = []
        for i in xrange(d):
            die = False
            while not die:
                row = randint(0, self.ch - 1)
                col = randint(0, self.cw - 1)
                if (row, col) not in tmp:
                    tmp.append((row, col))
                    self.move(row, col)
                    die = True

    def on_touch_down(self, touch):
        """Override on_touch_down event handler."""
        for c in self.children:
            res = c.dispatch('on_touch_down', touch)
            if res:
                row, col = c.crow, c.ccol
                self.move(row, col)


def button_on_release_factory(board, difficulty):
    """Generate events handler for kivi.uix.button.Button."""
    def res(self):
        board.clear()
        board.generate_level(difficulty)
    return res


class Comp(App):
    """Main class for the App."""

    def build(self):
        """Override build method."""
        mainl = StackLayout(orientation="lr-tb")

        b = Board(10, 5)
        b.size_hint = (1, 0.8)

        commands = BoxLayout(orientation="horizontal")

        easy = Button(text="Easy")
        callback = button_on_release_factory(b, 5)
        easy.bind(on_release=callback)

        medium = Button(text="Medium")
        callback = button_on_release_factory(b, 10)
        medium.bind(on_release=callback)

        hard = Button(text="Hard")
        callback = button_on_release_factory(b, 23)
        hard.bind(on_release=callback)

        commands.add_widget(easy)
        commands.add_widget(medium)
        commands.add_widget(hard)
        commands.size_hint = (1, 0.2)

        mainl.add_widget(b)
        mainl.add_widget(commands)

        return mainl

if __name__ == "__main__":
    """is *really* a comment needed?"""
    Comp().run()
